import 'package:epcapp/helper/ColorHelper.dart';
import 'package:epcapp/models/models.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class BasicReservationCard extends StatelessWidget {
  final Reservation reservation;

  BasicReservationCard({this.reservation});

  @override
  Widget build(BuildContext context) {
    return Card(
        elevation: 2.5,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(6)),
        ),
        margin: EdgeInsets.symmetric(horizontal: 15.0, vertical: 18.0),
        child: InkWell(
          onTap: () => print('tapped button'),
          splashColor: hexToColor('#ec695c'),
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(6), bottomLeft: Radius.circular(6.0)),
          child: ClipRRect(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(6), bottomLeft: Radius.circular(6.0)),
            child: Row(
              children: <Widget>[
                Container(
                  width: 65.0,
                  color: hexToColor('#0c1e34'),
                  padding:
                      EdgeInsets.symmetric(vertical: 20.0, horizontal: 10.0),
                  child: Column(
                    children: <Widget>[
                      Text(
                        DateFormat('MMM')
                            .format(reservation.eventDate)
                            .toString(),
                        style: TextStyle(fontSize: 14.0, color: Colors.white),
                      ),
                      Text(reservation.eventDate.day.toString(),
                          style: TextStyle(fontSize: 40.0, color: Colors.white))
                    ],
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(left: 11.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                          Text(
                            reservation.eventTitle,
                            style: TextStyle(
                                fontSize: 16.0,
                                color: hexToColor('#0c1e34'),
                                letterSpacing: 0.0),
                          ),
                        ]),
                      ),
                      Container(
                        padding: EdgeInsets.only(left: 11.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              reservation.eventLocation,
                              style: TextStyle(

                                  fontSize: 14.0, color: hexToColor('#9b9b9b')),
                            )
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(left: 11.0),
//                        margin: EdgeInsets.only(right: 6.0),
                        child: Row(
//                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            Text('${DateFormat('EEE').format(reservation.eventDate)}  |  ${DateFormat('hh:mmaa').format(reservation.eventDate)}'),
                            SizedBox(width: 110.0,),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[
                                // TODO: never use hard coded currencies. abstract this away
                                Text(NumberFormat.currency(locale: 'en_US', symbol: '\$').format(reservation.unitPrice),
                                  style: TextStyle(
                                    fontSize: 24.0,
                                    color: hexToColor('#0c1e34'),
                                    letterSpacing: -0.44
                                  ),
                                ),
                                Text(
                                  'Per Person',
                                  style: TextStyle(
                                    fontSize: 11.0,
                                    letterSpacing: 0.0,
                                    color: hexToColor('#0c1e34')
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                      )
                    ],
                  ),
              ],
            ),
          ),
        )

        );
  }
}
