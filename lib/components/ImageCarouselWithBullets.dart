import 'package:carousel_slider/carousel_slider.dart';
import 'package:epcapp/helper/ColorHelper.dart';
import 'package:epcapp/models/ImageCarouselItem.dart';
import 'package:flutter/material.dart';

List<T> map<T>(List list, Function handler) {
  List<T> result = [];
  for (var i = 0; i < list.length; i++) {
    result.add(handler(i, list[i]));
  }

  return result;
}

class ImageCarousel extends StatefulWidget {
  final List<ImageCarouselItem> images;

  const ImageCarousel({Key key, this.images}) : super(key: key);

  @override
  _ImageCarouselState createState() => _ImageCarouselState();
}

class _ImageCarouselState extends State<ImageCarousel> {
  int _current = 0;
  String description;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(38.0, 59.0, 37.0, 47.0),
      child: Container(
        height: 600,
        child: Stack(
          children: <Widget>[
            if (widget.images[_current].description != null)
              Text(widget.images[_current].description,
                  style: TextStyle(
                      color: const Color(0xff231f38),
                      fontWeight: FontWeight.w600,
                      fontFamily: "AvenirNext",
                      fontStyle: FontStyle.normal,
                      fontSize: 24.0),
                  textAlign: TextAlign.center),
            CarouselSlider(
              height: 590,
              aspectRatio: 9 / 16,
              enableInfiniteScroll: false,
              viewportFraction: 0.9,
              onPageChanged: (index) {
                setState(() {
                  this._current = index;
                });
              },
              items: widget.images.map((item) => item.image).toList(),
            ),
            Positioned(
                top: 581.0,
                left: 0.0,
                right: 0.0,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: map<Widget>(widget.images, (index, url) {
                    return Container(
                      width: 9.0,
                      height: 9.0,
                      margin:
                          EdgeInsets.symmetric(vertical: 10.0, horizontal: 4.0),
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: _current == index
                              ? hexToColor('#231f38')
                              : Colors.grey[300]),
                    );
                  }),
                )),
          ],
        ),
      ),
    );
  }
}
