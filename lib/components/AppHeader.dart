import 'package:epcapp/helper/ColorHelper.dart';
import 'package:flutter/material.dart';

class AppHeader extends StatelessWidget implements PreferredSizeWidget {
  final String headerValue;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Column(children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Hero(
                tag: 'topBarBtn',
                child: Text(headerValue,
                  style: TextStyle(
                    fontSize: 28.0,
                    letterSpacing: 0,
                    color: hexToColor('#0c1e34')
                  ),),
                )
            ],
          ),
        ]));
  }

  @override
  // TODO: implement preferredSize
  final Size preferredSize;

  AppHeader({this.headerValue}) : preferredSize = Size.fromHeight(65.0);

}
