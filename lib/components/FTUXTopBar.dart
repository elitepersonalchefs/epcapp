import 'package:flutter/material.dart';

class FtuxAppBar extends StatelessWidget implements PreferredSizeWidget {
  @override
  final Size preferredSize;
  final String title;
  final Widget child;
  final Function onPressed;
  final Function onTitleTapped;
  final Function onBackButtonPressed;

  FtuxAppBar(
      {this.title,
      this.child,
      this.onPressed,
      this.onBackButtonPressed,
      this.onTitleTapped})
      : preferredSize = Size.fromHeight(60.0);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Column(children: <Widget>[
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Hero(
            tag: 'topBarBtn',
            child: Card(
                elevation: 0,
                child: IconButton(
                  icon: Icon(Icons.arrow_back_ios),
                  color: Color(0xff4a4a4a),
                  onPressed: onBackButtonPressed,
                )),
          )
        ],
      ),
    ]));
  }
}
