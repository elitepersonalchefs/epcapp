import 'package:epcapp/helper/ColorHelper.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

class FtuxBottomBar extends StatelessWidget {
  final Function onPressed;
  final String buttonText;
  final String buttonColor;
  final String textColor;

  FtuxBottomBar(
      {@required this.onPressed,
      @required this.buttonColor,
      @required this.textColor,
      @required this.buttonText});

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.fromLTRB(23, 11.5, 23, 30),
        child: ButtonTheme(
            height: 50,
            child: RaisedButton(
              child: Text(
                buttonText,
                style: TextStyle(
                    color: hexToColor(textColor),
                    fontSize: 16,
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.w600),
              ),
              color: hexToColor(buttonColor),
              autofocus: true,
              onPressed: onPressed,
            )));
  }
}
