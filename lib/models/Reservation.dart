import 'package:equatable/equatable.dart';

class Reservation extends Equatable {
  final DateTime eventDate;
  final String eventLocation;
  final String eventTitle;
  final String userId;
  final ReservationStatus status;
  final double unitPrice;
  final double amountSpent;

  String get listLocation {
    if (status == ReservationStatus.Canceled) {
      return 'Canceled Reservations ';
    }

    switch(eventDate.compareTo(DateTime.now())){
      case 1:
      case 0:{
        return 'Upcoming Reservations';
      }
      case -1:{
        return 'Your Past Reservations';
      }
      default:
        return 'Your past reasdfa';
    }
  }

  Reservation({this.eventLocation,
    this.eventTitle,
    this.eventDate,
    this.status,
    this.unitPrice,
    this.amountSpent,
    this.userId});

  @override
  List<Object> get props =>
      [eventDate, userId, status, eventLocation, eventTitle];
}

enum ReservationStatus { Active, Canceled }
