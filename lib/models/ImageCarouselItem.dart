import 'package:flutter/material.dart';

class ImageCarouselItem {
  Widget image;
  String description;

  ImageCarouselItem({this.description, this.image});
}
