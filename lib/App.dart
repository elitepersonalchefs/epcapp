import 'package:epcapp/BLoC/blocs/AuthenticationBloc.dart';
import 'package:epcapp/BLoC/repositories/UserRepository.dart';
import 'package:epcapp/BLoC/state/AuthenticationState.dart';
import 'package:epcapp/screens/Dashboard/dart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'screens/Auth/AuthScreens.dart';

class App extends StatelessWidget {
  final UserRepository userRepository;

  App({Key key, @required this.userRepository})
      : assert(userRepository != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
//      initialRoute:
//      '/initialSignUp/authSignupSelection/EmailSelection/NameSelection/CreatePassword',
      debugShowCheckedModeBanner: false,
      routes: {
        '/initialSignUp': (context) => SignUp(),
        '/initialSignUp/authSignupSelection': (context) =>
            SignUpAuthSelection(),
        '/initialSignUp/authSignupSelection/NameSelection': (context) =>
            NameSelection(),
        '/initialSignUp/authSignupSelection/EmailSelection': (context) =>
            EmailSelectionScreen(),
        '/initialSignUp/authSignupSelection/EmailSelection/NameSelection/CreatePassword':
            (context) => CreatePassword(),
        '/initialSignUp/authSignupSelection/NameSelection/NameSelection/ProfilePhotoUpload':
            (context) => ProfilePhotoUpload(),
        '/initialSignUp/authSignupSelection/NameSelection/NameSelection/ProfilePhotoUpload/LocationSelection':
            (context) => LocationSelection(),
        '/Dashboard': (context) => Dashboard()
      },
      home: BlocBuilder<AuthenticationBloc, AuthenticationState>(
        builder: (context, state) {
          if (state is UninitializedAuthState ||
              state is UnauthenticatedState) {
            return SignUp();
          }
          return Dashboard();
        },
      ),
    );
  }
}
