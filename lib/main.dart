import 'package:epcapp/App.dart';
import 'package:epcapp/BLoC/SimpleDelegate.dart';
import 'package:epcapp/BLoC/blocs/AuthenticationBloc.dart';
import 'package:epcapp/BLoC/repositories/UserRepository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'BLoC/events/AuthenticationEvent.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  BlocSupervisor.delegate = SimpleDelegate();
  final UserRepository userRepository = new UserRepository();
  runApp(BlocProvider(
      create: (context) => AuthenticationBloc(
            userRepository: userRepository,
          )..add(AppStarted()),
      child: App(userRepository: userRepository))
  );
}
