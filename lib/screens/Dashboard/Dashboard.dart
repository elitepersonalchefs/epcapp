import 'package:epcapp/helper/ColorHelper.dart';
import 'package:epcapp/screens/Account/account.dart';
import 'package:epcapp/screens/Chefs/chefs.dart';
import 'package:epcapp/screens/Messages/messages.dart';
import 'package:epcapp/screens/Reservations/reservations.dart';
import 'package:flutter/material.dart';

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  int _currentIndex = 0;
  final List<Widget> _children = [
    Center(
      child: RaisedButton(
        child: Text('Go To Sign up'),
        onPressed: () {
//          Navigator.pushNamed(context, '/initialSignUp');
        },
      ),
    ),
    ChefSelector(),
    ReservationList(),
    MessageList(),
    AccountScreen()
  ];

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  Color _handleTextColor(int currentIndex, int navigationItemIndex) {
    if (currentIndex == navigationItemIndex) {
      return hexToColor('#eb685c');
    }
    return hexToColor('#bfbfbf');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _children[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        type: BottomNavigationBarType.fixed,
        onTap: onTabTapped,
        showUnselectedLabels: true,
        showSelectedLabels: true,
        items: [
          BottomNavigationBarItem(
              icon: ImageIcon(
                AssetImage('assets/images/icon_logo.png'),
                color: _handleTextColor(_currentIndex, 0),
              ),
              title: Text(
                'Events',
                style: TextStyle(
                  color: _handleTextColor(_currentIndex, 0),
                  fontSize: 10.0,
                ),
              )),
          BottomNavigationBarItem(
              icon: ImageIcon(
                AssetImage('assets/images/chef_hat.png'),
                color: _handleTextColor(_currentIndex, 1),
              ),
              title: Text(
                'Chefs',
                style: TextStyle(
                  color: _handleTextColor(_currentIndex, 1),
                  fontSize: 10.0,
                ),
              )),
          BottomNavigationBarItem(
              icon: ImageIcon(
                AssetImage('assets/images/calendar_1.png'),
                color: _handleTextColor(_currentIndex, 2),
              ),
              title: Text('Reservations',
                  style: TextStyle(
                    color: _handleTextColor(_currentIndex, 2),
                    fontSize: 10.0,
                  ))),
          BottomNavigationBarItem(
              icon: ImageIcon(
                AssetImage('assets/images/envelope_2.png'),
                color: _handleTextColor(_currentIndex, 3),
              ),
              title: Text('Messages',
                  style: TextStyle(
                    color: _handleTextColor(_currentIndex, 3),
                    fontSize: 10.0,
                  ))),
          BottomNavigationBarItem(
              icon: ImageIcon(
                AssetImage('assets/images/account_icon.png'),
                color: _handleTextColor(_currentIndex, 4),
              ),
              title: Text('Account',
                  style: TextStyle(
                    color: _handleTextColor(_currentIndex, 4),
                    fontSize: 10.0,
                  )))
        ],
      ),
    );
  }
}
