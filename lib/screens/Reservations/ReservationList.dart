import 'package:epcapp/components/Reservation/BasicReservationCard.dart';
import 'package:epcapp/helper/ColorHelper.dart';
import 'package:epcapp/models/models.dart';
import 'package:flutter/material.dart';
import 'package:grouped_list/grouped_list.dart';

class ReservationList extends StatefulWidget {
  final List<Reservation> reservations = [
    Reservation(
      eventDate: DateTime.now().add(Duration(days:-1)),
      eventTitle: 'Summer Dinner Party',
      eventLocation: 'Las Vegas, NV',
      unitPrice: 125.0,
      amountSpent: 125.0,
      status: ReservationStatus.Active
    ),
    Reservation(
      eventDate: DateTime.now().add(Duration(days:-14)),
      eventTitle: 'Classic Hearty Brunch',
      eventLocation: 'Chicago, IL',
      unitPrice: 50,
      amountSpent: 50,
      status: ReservationStatus.Canceled
    ),
    Reservation(
        eventDate: DateTime.now().add(Duration(days:1)),
        eventTitle: 'Taste of Fall Dinner',
        eventLocation: 'New York, NY',
        unitPrice: 150,
        amountSpent: 150,
        status: ReservationStatus.Active
    ),
    Reservation(
        eventDate: DateTime.now().add(Duration(days:5)),
        eventTitle: 'Summer Dinner Party',
        eventLocation: 'Las Vegas, NV',
        unitPrice: 125,
        amountSpent: 125,
        status: ReservationStatus.Active
    ),
    Reservation(
        eventDate: DateTime.now().add(Duration(days:-3)),
        eventTitle: 'A Night in the Mediterranean',
        eventLocation: 'Chicago, IL',
        unitPrice: 50,
        amountSpent: 50,
        status: ReservationStatus.Active
    ),
    Reservation(
        eventDate: DateTime.now().add(Duration(days:2)),
        eventTitle: 'A Tour of Thailand',
        eventLocation: 'New York, NY',
        unitPrice: 75,
        amountSpent: 75,
        status: ReservationStatus.Active
    )
  ];
  ReservationList({Key key}) : super(key: key);
  @override
  _ReservationListState createState() => _ReservationListState();
}

class _ReservationListState extends State<ReservationList> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          automaticallyImplyLeading: false,
          actions: <Widget>[
            IconButton(
              icon: Icon(
                Icons.search,
                color: Colors.black,
              ),
              onPressed: () => print('clicked search'),
            ),
            IconButton(
              icon: Icon(
                Icons.notifications_none,
                color: Colors.black,
              ),
              onPressed: () => print('clicked notification'),
            )
          ],
          title: Text(
            'Reservations',
            style: TextStyle(color: Colors.black, fontSize: 28.0),
          ),
          backgroundColor: Colors.white,
        ),
        body:_showList()
    );
  }

  Widget _showList(){
    if(widget.reservations != null && widget.reservations.length > 0){
      return _populatedList(widget.reservations);
    }
    return _emptyList();
  }

  Widget _emptyList() {
    return Center(
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 78.0,
          ),
          Image.asset(
            'assets/images/reservation_image.png',
            width: 308.0,
            height: 235.0,
          ),
          Text(
            'No reservations yet.',
            style: TextStyle(
                fontSize: 24.0,
                fontWeight: FontWeight.w600,
                color: hexToColor('#1f263e')),
          ),
          Text(
            'Check out events',
            style: TextStyle(
                fontSize: 24.0,
                fontWeight: FontWeight.w600,
                color: hexToColor('#1f263e')),
          ),
          Text(
            'happening around you!',
            style: TextStyle(
                fontSize: 24.0,
                fontWeight: FontWeight.w600,
                color: hexToColor('#1f263e')),
          )
        ],
      ),
    );
  }

  Widget _populatedList(List<Reservation> items) =>
      GroupedListView<Reservation, String>(
        groupBy: (Reservation el) => el.listLocation,
        elements: items,
        groupSeparatorBuilder: (String value) => Padding(
          padding: EdgeInsets.fromLTRB(15.0, 3.0, 8.0, 0),
            child: Text(value),
        ),
        itemBuilder: (context, Reservation element){
          return BasicReservationCard(reservation: element);
        },
      );
}
