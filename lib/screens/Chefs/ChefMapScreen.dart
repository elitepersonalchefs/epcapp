import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class ChefMapScreen extends StatefulWidget {
  @override
  _ChefMapScreenState createState() => _ChefMapScreenState();
}

class _ChefMapScreenState extends State<ChefMapScreen> {
  GoogleMapController _mapController;
  final LatLng _center = const LatLng(45.521563, -122.677433);
  void _onMapCreated(GoogleMapController controller) {
    _mapController = controller;
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        GoogleMap(
          onMapCreated: _onMapCreated,
          initialCameraPosition: CameraPosition(
            target: _center,
            zoom: 11.0
          ),
        ),
        Positioned(
          bottom: 15,
          left: 0.0,
          right: 0.0,
          child: IconButton(
            iconSize: 97.0,
            color: Colors.white,
            icon: ImageIcon(AssetImage('assets/images/filter_btn.png')),
          ),
        )
      ],
    );
  }
}
