import 'package:epcapp/helper/ColorHelper.dart';
import 'package:flutter/material.dart';

class ChefListScreen extends StatefulWidget {
  @override
  _ChefListScreenState createState() => _ChefListScreenState();
}

class _ChefListScreenState extends State<ChefListScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _emptyList(),
    );
  }

  Widget _emptyList() {
    return Center(
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 78.0,
          ),
          Image.asset(
            'assets/images/no_chef.png',
            width: 308.0,
            height: 235.0,
          ),
          Text(
            'There are no chefs hosting',
            style: TextStyle(
                fontSize: 24.0,
                fontWeight: FontWeight.w600,
                color: hexToColor('#1f263e')),
          ),
          Text(
            ' events in Reston, VA.',
            style: TextStyle(
                fontSize: 24.0,
                fontWeight: FontWeight.w600,
                color: hexToColor('#1f263e')),
          ),
          Text(
            'Interested in hosting or know a great host?',
            style: TextStyle(
                fontSize: 14.0,
                fontWeight: FontWeight.normal,
                color: hexToColor('#1f263e')),
          ),
          Text(
            'Refer them here and earn \$50 / chef.',
            style: TextStyle(
                color: const Color(0xffeb685c),
                fontWeight: FontWeight.w500,
                fontFamily: "AvenirNext",
                fontStyle: FontStyle.normal,
                fontSize: 14.0),
          )
        ],
      ),
    );
  }
}
