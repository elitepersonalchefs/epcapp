import 'package:epcapp/helper/ColorHelper.dart';
import 'package:epcapp/screens/Chefs/chefs.dart';
import 'package:flutter/material.dart';

class ChefSelector extends StatefulWidget {
  @override
  _ChefSelectorState createState() => _ChefSelectorState();
}

class _ChefSelectorState extends State<ChefSelector> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          elevation: 0,
          automaticallyImplyLeading: false,
          actions: <Widget>[
            IconButton(
              icon: Icon(
                Icons.notifications_none,
                color: Colors.black,
              ),
              onPressed: () => print('clicked notification'),
            )
          ],
          title: Text(
            'Chefs',
            style: TextStyle(color: Colors.black, fontSize: 28.0),
          ),
          backgroundColor: hexToColor('#f7f8fb'),
          bottom: TabBar(
            indicatorColor: hexToColor('#0c1e34'),
            indicatorWeight: 3.0,
            indicatorSize: TabBarIndicatorSize.label,
            tabs: <Widget>[
              Text('Map View', style: TextStyle(color: hexToColor('#0c1e34'))),
              Text('List View', style: TextStyle(color: hexToColor('#0c1e34')))
            ],
          ),
        ),
        body: TabBarView(
          children: <Widget>[
            ChefMapScreen(),
            ChefListScreen()
          ],
        ),
      ),
    );
  }
}
