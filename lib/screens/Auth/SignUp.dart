import 'package:epcapp/components/ImageCarouselWithBullets.dart';
import 'package:epcapp/helper/ColorHelper.dart';
import 'package:epcapp/models/ImageCarouselItem.dart';
import 'package:flutter/material.dart';

class SignUp extends StatelessWidget {
  final List<ImageCarouselItem> imgList = [
    new ImageCarouselItem(
        image: Image(
          image: AssetImage('assets/images/undraw_people_search_wctu.png'),
        ),
        description: 'Browse culinary events happening around you'),
    ImageCarouselItem(
        image: Image(
          image: AssetImage('assets/images/undraw_people_search_wctu.png'),
        ),
        description: 'See an event you like? Book your reservation in minutes!'),
    ImageCarouselItem(
        image: Image(
          image: AssetImage('assets/images/undraw_people_search_wctu.png'),
        ),
        description:
            'Need a custom package? Contact your chef directly!'),
    ImageCarouselItem(
        image: Image(
          image: AssetImage('assets/images/undraw_people_search_wctu.png'),
        ),
        description:
        'Enjoy the experience! It\'s as simple as that')
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: hexToColor('#ffffff'),
      body: SafeArea(
          child: Column(
        children: <Widget>[
          Expanded(
            child: ImageCarousel(
              images: imgList,
            ),
          ),
          FlatButton(
            splashColor: Colors.white,
            onPressed: () {
              print('clicked the sign in link');
            },
            child: Text('Been here before? Sign In'),
          )
        ],
      )),
      bottomNavigationBar: Container(
        color: hexToColor('#ffffff'),
        child: Padding(
          padding: EdgeInsets.fromLTRB(23, 11.5, 23, 30),
          child: ButtonTheme(
            height: 50,
            child: RaisedButton(
              child: Text(
                'Sign Up',
                style: TextStyle(
                    color: hexToColor('#ffffff'),
                    fontSize: 16,
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.w600),
              ),
              color: hexToColor('#ec695c'),
              onPressed: () {
                Navigator.pushNamed(
                    context, '/initialSignUp/authSignupSelection');
              },
            ),
          ),
        ),
      ),
    );
  }
}
