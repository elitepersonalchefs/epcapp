import 'package:epcapp/BLoC/blocs/AuthenticationBloc.dart';
import 'package:epcapp/BLoC/events/AuthenticationEvent.dart';
import 'package:epcapp/components/FTUXTopBar.dart';
import 'package:epcapp/helper/ColorHelper.dart';
import 'package:epcapp/screens/Auth/NameSelection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class EmailSelectionScreen extends StatefulWidget {
  @override
  _EmailSelectionScreenState createState() => _EmailSelectionScreenState();
}

class _EmailSelectionScreenState extends State<EmailSelectionScreen> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _emailController = TextEditingController();
  bool _submitButtonDisabled = true;
  AuthenticationBloc _authenticationBloc;

  void _handleButtonPress() async {
    _authenticationBloc.add(EmailChanged(email: _emailController.text));
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => NameSelection()));
  }

  @override
  void initState() {
    super.initState();
    _authenticationBloc = BlocProvider.of<AuthenticationBloc>(context);
    _emailController.addListener(onEmailChanged);
  }

  void onEmailChanged() {

    print('$_submitButtonDisabled for ${_emailController.text}');

  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: hexToColor('#ffffff'),
      body: Padding(
        padding: const EdgeInsets.only(left: 21.0, right: 21.0),
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 40.0,
            ),
            SizedBox(
              width: 330,
              height: 58,
              child: Text('What’s your email?',
                  textAlign: TextAlign.left,
                  style: const TextStyle(
                      color: const Color(0xff4a4a4a),
                      fontWeight: FontWeight.w600,
                      fontFamily: "AvenirNext",
                      fontStyle: FontStyle.normal,
                      fontSize: 28.0)),
            ),
            SizedBox(
              width: 322,
              height: 36,
              child: Text(
                'We’ll send you a confirmation, make sure to verify your account.',
                style: TextStyle(
                    color: hexToColor('#6a696a'),
                    fontWeight: FontWeight.w600,
                    fontFamily: "AvenirNext",
                    fontStyle: FontStyle.normal,
                    fontSize: 13.0),
              ),
            ),
            SizedBox(
              height: 39.0,
            ),
            Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  SizedBox(
                    width: 325.0,
                    child: TextFormField(
                      controller: _emailController,
                      autovalidate: true,
                      initialValue: null,
                      autocorrect: false,
                      autofocus: true,
                      validator: (String value) {
                        Pattern pattern =
                            r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                        RegExp regex = new RegExp(pattern);
                        if (!regex.hasMatch(value) && value.isNotEmpty)
                          return 'Enter Valid Email';
                        else
                          return null;
                      },
                      keyboardType: TextInputType.emailAddress,
                      decoration: InputDecoration(
                          labelText: 'Email',
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(7.0))),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      appBar: FtuxAppBar(
        onBackButtonPressed: () {
          Navigator.pop(context);
        },
      ),
      bottomNavigationBar: Container(
        color: hexToColor('#ffffff'),
        child: Padding(
          padding: EdgeInsets.fromLTRB(23, 11.5, 23, 30),
          child: ButtonTheme(
            height: 50,
            child: RaisedButton(
              child: Text(
                'Next',
                style: TextStyle(
                    color: hexToColor('#ffffff'),
                    fontSize: 16,
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.w600),
              ),
              color: const Color(0xffec695c),
              autofocus: true,
              onPressed: _submitButtonDisabled
                      ? null
                      : _handleButtonPress,
            ),
          ),
        ),
      ),
    );
  }
}
