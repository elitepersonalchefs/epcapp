import 'dart:io';

import 'package:epcapp/components/FTUXBottomBar.dart';
import 'package:epcapp/components/FTUXTopBar.dart';
import 'package:epcapp/helper/ColorHelper.dart';
import 'package:epcapp/screens/Auth/AuthScreens.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class ProfilePhotoUpload extends StatefulWidget {
  @override
  _ProfilePhotoUploadState createState() => _ProfilePhotoUploadState();
}

class _ProfilePhotoUploadState extends State<ProfilePhotoUpload> {
  PersistentBottomSheetController _bottomSheetController;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Padding(
          padding: const EdgeInsets.only(left: 21.0),
          child: Column(
            children: <Widget>[
              Align(
                alignment: Alignment.centerLeft,
                child: Text('Profile Picture',
                    textAlign: TextAlign.left,
                    style: const TextStyle(
                        color: const Color(0xff4a4a4a),
                        fontWeight: FontWeight.w600,
                        fontFamily: "AvenirNext",
                        fontStyle: FontStyle.normal,
                        fontSize: 28.0)),
              ),
              SizedBox(
                height: 6.0,
              ),
              Text(
                'Other users and chefs will see this when you review the chefs! You can always change it later.',
                style: TextStyle(
                    letterSpacing: 0.0,
                    fontSize: 13.0,
                    color: hexToColor('#6a696a')),
                textAlign: TextAlign.left,
              )
            ],
          ),
        ),
        appBar: FtuxAppBar(
          onBackButtonPressed: () {
            Navigator.pop(context);
          },
        ),
        bottomNavigationBar: Container(
            color: hexToColor('#ffffff'),
            child: Builder(
              builder: (bCtx) {
                return FtuxBottomBar(
                  textColor: '#ffffff',
                  buttonText: 'Upload',
                  buttonColor: '#ec695c',
                  onPressed: () {
                    _bottomSheetController =
                        Scaffold.of(bCtx).showBottomSheet((context) {
                      return Container(
                        height: 162.0,
                        child: Wrap(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(left: 8.0),
                              child: Text(
                                'Choose Photo',
                                style: TextStyle(
                                    color: hexToColor('#4a4a4a'), fontSize: 21),
                              ),
                            ),
                            Card(
                              child: ListTile(
                                title: Text('Take a Photo'),
                                onTap: () async {
                                  print('take pic');
                                  File image = await ImagePicker.pickImage(
                                      source: ImageSource.camera);
                                  _bottomSheetController.close();
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              ProfilePhotoConfirm(
                                                image: image,
                                              )));

                                },
                              ),
                            ),
                            Card(
                              child: ListTile(
                                title: Text('Choose Existing'),
                                onTap: () async {
                                  print('choose pic');
                                  File image = await ImagePicker.pickImage(
                                      source: ImageSource.gallery);
                                  _bottomSheetController.close();
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              ProfilePhotoConfirm(
                                                image: image,
                                              )));
                                },
                              ),
                            ),
                            ListTile(
                              title: Text(
                                'Cancel',
                                style: TextStyle(
                                  fontSize: 16.0,
                                ),
                              ),
                            )
                          ],
                        ),
                      );
                    });
                  },
                );
              },
            )));
  }
}
