import 'package:epcapp/BLoC/blocs/AuthenticationBloc.dart';
import 'package:epcapp/BLoC/events/AuthenticationEvent.dart';
import 'package:epcapp/BLoC/repositories/UserRepository.dart';
import 'package:epcapp/BLoC/state/AuthenticationState.dart';
import 'package:epcapp/components/FTUXTopBar.dart';
import 'package:epcapp/helper/ColorHelper.dart';
import 'package:epcapp/screens/Auth/NameSelection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SignUpAuthSelection extends StatefulWidget {
  @override
  _SignUpAuthSelectionState createState() => _SignUpAuthSelectionState();
}

class _SignUpAuthSelectionState extends State<SignUpAuthSelection> {
  AuthenticationBloc _authenticationBloc;

  @override
  void initState() {
    super.initState();
    _authenticationBloc = BlocProvider.of<AuthenticationBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthenticationBloc, AuthenticationState>(
      bloc: _authenticationBloc,
      child: Scaffold(
        backgroundColor: Color(0xffffffff),
        appBar: FtuxAppBar(
          onBackButtonPressed: () {
            Navigator.pop(context);
          },
        ),
        body: Center(
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 50.0,
              ),
              Text(
                'Let’s Sign Up',
                textAlign: TextAlign.left,
                style: TextStyle(
                    color: const Color(0xff4a4a4a),
                    fontWeight: FontWeight.w600,
                    fontFamily: "Poppins",
                    fontStyle: FontStyle.normal,
                    fontSize: 28.0),
              ),
              SizedBox(
                height: 15.0,
              ),
              Container(
                child: ButtonTheme(
                  minWidth: 330,
                  height: 50,
                  child: RaisedButton.icon(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(6)),
                    ),
                    color: hexToColor('#4568b2'),
                    icon: Image.asset('assets/images/facebook_logo.png'),
                    onPressed: () async {
                      _authenticationBloc
                          .add(LoggingIn(loginType: LoginType.Facebook));
                    },
                    label: Text(
                      'Continue with Facebook',
                      style: TextStyle(
                          color: Color(0xffffffff),
                          fontWeight: FontWeight.w600,
                          fontFamily: "Poppins",
                          fontStyle: FontStyle.normal,
                          fontSize: 14.0),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 7.0,
              ),
              ButtonTheme(
                minWidth: 330,
                height: 50,
                child: RaisedButton.icon(
                  color: Color(0xffffffff),
                  onPressed: () async {
                    _authenticationBloc
                        .add(LoggingIn(loginType: LoginType.Google));
//                    Navigator.push(
//                        context,
//                        MaterialPageRoute(
//                            builder: (context) =>
//                                NameSelection(firstName: 'test')));

//                  // TODO: route to user first name page, and pass username to page
//                  return user;
                  },
                  icon: Image.asset('assets/images/google_favicon_logo.png'),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(6)),
                  ),
                  label: Text(
                    'Continue with Google',
                    style: TextStyle(
                        color: hexToColor('#4a4a4a'),
                        fontWeight: FontWeight.w600,
                        fontFamily: "Poppins",
                        fontStyle: FontStyle.normal,
                        fontSize: 14.0),
                  ),
                ),
              ),
              SizedBox(
                height: 7.0,
              ),
              ButtonTheme(
                minWidth: 330,
                height: 50,
                child: RaisedButton(
                  color: hexToColor('#ec695c'),
                  onPressed: () async {
                    Navigator.pushNamed(context,
                        '/initialSignUp/authSignupSelection/EmailSelection');
                  },
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(6)),
                  ),
                  child: Text(
                    'Continue with Email',
                    style: TextStyle(
                        color: Color(0xffffffff),
                        fontWeight: FontWeight.w600,
                        fontFamily: "Poppins",
                        fontStyle: FontStyle.normal,
                        fontSize: 14.0),
                  ),
                ),
              ),
              SizedBox(
                height: 7.0,
              ),
              Container(
                width: 302.0,
                height: 55,
                padding: EdgeInsets.only(top: 12.0),
                child: Text(
                    'By tapping Continue with Facebook, Continue with Google or Continue with email, you agree to the “terms of Use” and Privacy Policy.',
                    style: TextStyle(
                        color: const Color(0xff9b9b9b),
                        fontWeight: FontWeight.w400,
                        fontFamily: "Poppins",
                        fontStyle: FontStyle.normal,
                        fontSize: 12.0)),
              )
            ],
          ),
        ),
        bottomNavigationBar: Container(
          color: hexToColor('#ffffff'),
          child: Padding(
            padding: EdgeInsets.fromLTRB(23, 11.5, 23, 30),
            child: FlatButton(
              splashColor: Colors.white,
              onPressed: () {
                print('clicked the sign in link');
              },
              child: Text('Been here before? Sign In'),
            ),
          ),
        ),
      ),
      listener: (BuildContext context, state) {
        if (state is AuthenticatedState) {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => NameSelection(
                      firstName: state.user.user?.displayName?.split(" ")[0])));
        }
      },
    );
  }
}
