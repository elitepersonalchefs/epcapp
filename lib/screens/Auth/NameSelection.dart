import 'package:epcapp/BLoC/blocs/AuthenticationBloc.dart';
import 'package:epcapp/BLoC/events/AuthenticationEvent.dart';
import 'package:epcapp/BLoC/repositories/UserRepository.dart';
import 'package:epcapp/components/FTUXTopBar.dart';
import 'package:epcapp/helper/ColorHelper.dart';
import 'package:epcapp/models/User.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class NameSelection extends StatefulWidget {
  final String firstName;
  final UserRepository repo = UserRepository();

  @override
  _NameSelectionState createState() => _NameSelectionState();

  NameSelection({Key key, this.firstName}) : super(key: key);
}

class _NameSelectionState extends State<NameSelection> {
  final _formKey = GlobalKey<_NameSelectionState>();
  final TextEditingController _nameController = TextEditingController();
  AuthenticationBloc _authenticationBloc;

  @override
  void initState() {
    super.initState();
    _authenticationBloc = BlocProvider.of<AuthenticationBloc>(context);
    _nameController.text =
        widget.firstName.isNotEmpty ? widget.firstName : null;
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<User>(
        stream: _authenticationBloc.userStream,
        builder: (context, AsyncSnapshot<User> snapshot) {
          return Scaffold(
            backgroundColor: hexToColor('#ffffff'),
            body: Padding(
              padding: const EdgeInsets.only(left: 21.0, right: 21.0),
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 40.0,
                  ),
                  SizedBox(
                    width: 330,
                    height: 38,
                    child: Text('Your First Name',
                        textAlign: TextAlign.left,
                        style: const TextStyle(
                            color: const Color(0xff4a4a4a),
                            fontWeight: FontWeight.w600,
                            fontFamily: "AvenirNext",
                            fontStyle: FontStyle.normal,
                            fontSize: 28.0)),
                  ),
                  SizedBox(
                    width: 322,
                    height: 36,
                    child: Text(
                      'This is something other users and chefs will see when you write reviews or have conversations. ',
                      style: TextStyle(
                          color: hexToColor('#6a696a'),
                          fontWeight: FontWeight.w600,
                          fontFamily: "AvenirNext",
                          fontStyle: FontStyle.normal,
                          fontSize: 13.0),
                    ),
                  ),
                  Form(
                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        SizedBox(
                          width: 325.0,
                          child: TextFormField(
                            controller: _nameController,
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'fuck right off';
                              } else {
                                return null;
                              }
                            },
                            decoration: InputDecoration(
                                labelText: 'First Name',
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(7.0))
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            appBar: FtuxAppBar(
              onBackButtonPressed: () {
                Navigator.pop(context);
              },
            ),
            bottomNavigationBar: Container(
              color: hexToColor('#ffffff'),
              child: Padding(
                padding: EdgeInsets.fromLTRB(23, 11.5, 23, 30),
                child: ButtonTheme(
                  height: 50,
                  child: RaisedButton(
                    child: Text(
                      'Next',
                      style: TextStyle(
                          color: hexToColor('#ffffff'),
                          fontSize: 16,
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w600),
                    ),
                    color: const Color(0xff4a4a4a),
                    onPressed: () {
                      _authenticationBloc.add(
                          FirstNameChanged(firstName: _nameController.text));
                      Navigator.pushNamed(context,
                          '/initialSignUp/authSignupSelection/NameSelection/NameSelection/ProfilePhotoUpload');
                    },
                  ),
                ),
              ),
            ),
          );
        });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _authenticationBloc.dispose();
    _nameController.dispose();
  }
}
