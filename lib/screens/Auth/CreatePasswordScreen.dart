import 'package:epcapp/components/FTUXBottomBar.dart';
import 'package:epcapp/components/FTUXTopBar.dart';
import 'package:epcapp/helper/ColorHelper.dart';
import 'package:flutter/material.dart';

class CreatePassword extends StatefulWidget {
  @override
  _CreatePasswordState createState() => _CreatePasswordState();
}

class _CreatePasswordState extends State<CreatePassword> {
  final _formKey = GlobalKey<_CreatePasswordState>();
  bool isChecked = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: hexToColor('#ffffff'),
      body: Padding(
        padding: const EdgeInsets.only(left: 21.0, right: 21.0),
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 40.0,
            ),
            SizedBox(
              width: 330,
              height: 58,
              child: Text('Create a password',
                  textAlign: TextAlign.left,
                  style: const TextStyle(
                      color: const Color(0xff4a4a4a),
                      fontWeight: FontWeight.w600,
                      fontFamily: "AvenirNext",
                      fontStyle: FontStyle.normal,
                      fontSize: 28.0)),
            ),
            Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  SizedBox(
                    width: 325.0,
                    child: TextFormField(
                      validator: (String value) {
                        //  TODO: add good password validation
                        if (value.isEmpty)
                          return 'Enter Valid Password';
                        else
                          return null;
                      },
                      keyboardType: TextInputType.visiblePassword,
                      decoration: InputDecoration(
                          labelText: 'Password',
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(7.0))),
                    ),
                  ),
                  Row(
                    children: <Widget>[
                      SizedBox(
                        width: 10.0,
                      ),
                      Checkbox(
                        value: isChecked,
                        onChanged: (value) {
                          setState(() {
                            this.isChecked = value;
                          });
                        },
                      ),
                      SizedBox(
                          width: 196,
                          height: 16,
                          child: Text("Save password",
                              style: TextStyle(
                                  color: hexToColor('#4a4a4a'),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "AvenirNext",
                                  fontStyle: FontStyle.normal,
                                  fontSize: 12.0)))
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
      appBar: FtuxAppBar(
        onBackButtonPressed: () {
          Navigator.pop(context);
        },
      ),
      bottomNavigationBar: Container(
        color: hexToColor('#ffffff'),
        child: FtuxBottomBar(
          buttonColor: '#ec695c',
          buttonText: 'Next',
          textColor: '#ffffff',
          onPressed: () {
            /// TODO: implement saving to firebase auth. Take previous email and first name
            Navigator.pushNamed(context,
                '/initialSignUp/authSignupSelection/NameSelection/NameSelection/ProfilePhotoUpload');
          },
        ),
      ),
    );
  }
}
