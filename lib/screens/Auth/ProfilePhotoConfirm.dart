import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:epcapp/BLoC/repositories/UserRepository.dart';
import 'package:epcapp/components/components.dart';
import 'package:epcapp/helper/ColorHelper.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart' as path;

class ProfilePhotoConfirm extends StatefulWidget {
  final File image;

  ProfilePhotoConfirm({this.image});

  @override
  _ProfilePhotoConfirmState createState() => _ProfilePhotoConfirmState();
}

class _ProfilePhotoConfirmState extends State<ProfilePhotoConfirm> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: FtuxAppBar(
        onBackButtonPressed: () {
          Navigator.pop(context);
        },
      ),
      body: Column(
        children: <Widget>[
          Text(
            'What do you think?',
            style: TextStyle(
                fontSize: 28.0,
                letterSpacing: 0.0,
                color: hexToColor("#0c1e34")),
          ),
          SizedBox(
            height: 22.0,
          ),
          Center(
            child: Container(
              width: 161.0,
              height: 161.0,
              decoration: new BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                      fit: BoxFit.cover, image: FileImage(widget.image))),
            ),
          )
        ],
      ),
      bottomNavigationBar: FtuxBottomBar(
        buttonText: "Looks Good",
        buttonColor: "#eb685c",
        textColor: "#ffffff",
        onPressed: () async {
          print('save that shit');
          // TODO: abstract all this shit away to a BLoC: maybe the Auth block and repository?
          final repo = UserRepository();
          final user = (await repo.getUser());
          StorageReference storageReference = FirebaseStorage.instance
              .ref()
              .child(
                  'profiles/${user.uid}/${path.basename(widget.image.path)}');

          StorageUploadTask uploadTask = storageReference.putFile(widget.image);
          // TODO: dispatch loading action, so we can show loading thingy(?)

          await uploadTask.onComplete;
          // TODO: Dispatch upload complete action

          final String download = await storageReference.getDownloadURL();
          // TODO: save profile picture, first name, user id, email to the database and the user collection, along with the "isCP" to false
          await Firestore.instance
              .collection('users')
              .document(user.uid)
              .setData({
            'userId': user.uid,
            'firstName': user.displayName.split(" ")[0],
            'email': user.email,
            'profilePicUrl': download,
            'isCP': false
          });

          // TODO: Remove loading modal

          Navigator.pushNamed(context,
              '/initialSignUp/authSignupSelection/NameSelection/NameSelection/ProfilePhotoUpload/LocationSelection');
        },
      ),
    );
  }
}
