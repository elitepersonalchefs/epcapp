import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:epcapp/BLoC/repositories/UserRepository.dart';
import 'package:epcapp/components/components.dart';
import 'package:epcapp/helper/ColorHelper.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:geolocator/geolocator.dart';

class LocationSelection extends StatefulWidget {
  @override
  _LocationSelectionState createState() => _LocationSelectionState();
}

class _LocationSelectionState extends State<LocationSelection> {
  FirebaseUser currentUser;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: FtuxAppBar(
        onBackButtonPressed: null,
      ),
      body: Padding(
        padding: const EdgeInsets.all(21.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'Finally, what\'s your zip code?',
              style: const TextStyle(
                  color: const Color(0xff0c1e34),
                  fontWeight: FontWeight.w600,
                  fontFamily: "AvenirNext",
                  fontStyle: FontStyle.normal,
                  fontSize: 28.0),
            ),
            Text(
              'We’ll show you culinary events happening around this location.',
              style: TextStyle(
                  fontWeight: FontWeight.w600,
                  color: hexToColor('#69727c'),
                  fontFamily: "AvenirNext",
                  fontStyle: FontStyle.normal,
                  fontSize: 13.0),
            ),
            PlacesAutocompleteField(
              apiKey: "AIzaSyCq2SPCtOewu-dzk5T44yB2pRmEzzCBcf4",
              mode: Mode.overlay,
              sessionToken: Uuid().generateV4(),
              onError: (e) {
                print(e);
              },
              trailingOnTap: () {},
              onChanged: (val) async {
//
                currentUser = await UserRepository().getUser();
                await Firestore.instance
                    .collection('users')
                    .document(currentUser.uid)
                    .updateData({
                  'currentLocationAddress': val
                });
              },
            ),
            FlatButton.icon(
              icon: Icon(Icons.location_on),
              label: Text('Use Current Location'),
              onPressed: () async {
                currentUser = await UserRepository().getUser();
                Position pos = await Geolocator()
                    .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
                await Firestore.instance
                    .collection('users')
                    .document(currentUser.uid)
                    .updateData({
                  'currentLocationLong': pos.longitude,
                  'currentLocationLat': pos.latitude
                });

                // TODO: navigate to dashboard

              },
            )
          ],
        ),
      ),
      bottomNavigationBar: FtuxBottomBar(
        buttonText: "Next",
        buttonColor: "#ec695c",
        textColor: "#ffffff",
        onPressed: (){
          Navigator.pushNamed(context, '/Dashboard');
        },
      ),
    );
  }
}
