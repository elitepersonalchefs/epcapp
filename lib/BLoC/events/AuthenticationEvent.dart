import 'package:epcapp/BLoC/repositories/UserRepository.dart';
import 'package:equatable/equatable.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:meta/meta.dart';

abstract class AuthenticationEvent extends Equatable {
  const AuthenticationEvent();

  @override
  List<Object> get props => [];
}

class AppStarted extends AuthenticationEvent {}

class LoggingIn extends AuthenticationEvent {
  final String username;
  final String password;
  final LoginType loginType;

  LoggingIn({this.username, this.password, @required this.loginType});
}

class LoggedIn extends AuthenticationEvent {
  final AuthResult user;

  LoggedIn({@required this.user});

  @override
  List<Object> get props => [user];
}

class LoggedOut extends AuthenticationEvent {}

class EmailChanged extends AuthenticationEvent {
  final String email;

  const EmailChanged({@required this.email});

  @override
  List<Object> get props => [email];

  @override
  String toString() => 'EmailChanged { email :$email }';
}

class PasswordChanged extends AuthenticationEvent {
  final String password;

  const PasswordChanged({@required this.password});

  @override
  List<Object> get props => [password];

  @override
  String toString() => 'PasswordChanged { password: $password }';
}

class FirstNameChanged extends AuthenticationEvent {
  final String firstName;

  const FirstNameChanged({@required this.firstName});

  @override
  List<Object> get props => [firstName];

  @override
  String toString() => 'First Name Changed to $firstName';
}

class Submitted extends AuthenticationEvent {
  final String email;
  final String password;

  const Submitted({
    @required this.email,
    @required this.password,
  });

  @override
  List<Object> get props => [email, password];

  @override
  String toString() {
    return 'Submitted { email: $email, password: $password }';
  }
}

class LoginWithGooglePressed extends AuthenticationEvent {}

class LoginWithCredentialsPressed extends AuthenticationEvent {
  final String email;
  final String password;

  const LoginWithCredentialsPressed({
    @required this.email,
    @required this.password,
  });

  @override
  List<Object> get props => [email, password];

  @override
  String toString() {
    return 'LoginWithCredentialsPressed { email: $email, password: $password }';
  }
}
