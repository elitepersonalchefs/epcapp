import 'dart:async';

import 'package:epcapp/BLoC/events/AuthenticationEvent.dart';
import 'package:epcapp/BLoC/repositories/UserRepository.dart';
import 'package:epcapp/BLoC/state/AuthenticationState.dart';
import 'package:epcapp/models/User.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  final UserRepository userRepository;
  final userStreamController = StreamController<User>.broadcast();

  User currUser;

  Sink<User> get userSink => userStreamController.sink;

  Stream<User> get userStream => userStreamController.stream;

  AuthenticationBloc({@required this.userRepository})
      : assert(userRepository != null) {
    userStreamController.stream.listen(_setUser);
  }

  void dispose() {
    userStreamController.close();
  }

  _setUser(User newUser) {
    currUser = newUser;
  }

  @override
  AuthenticationState get initialState => UninitializedAuthState();

  @override
  Stream<AuthenticationState> mapEventToState(
    AuthenticationEvent event,
  ) async* {
    if (event is AppStarted) {
      final user = await userRepository.getUser();
      bool isLoggedIn = user != null;
      if (isLoggedIn && user.displayName.isNotEmpty) {
        userSink.add(
            User(email: user.email, firstName: user.displayName.split(" ")[0]));
        yield AuthenticatedState();
      } else {
        yield UnauthenticatedState();
      }
    }

    if (event is LoggingIn) {
      try {
        yield AuthLoadingState();
        final AuthResult user = await userRepository.login(
            event.loginType, event.username, event.password);
        yield AuthenticatedState(user: user);
      } on Exception catch (e) {
        yield AuthenticationFailedState(error: e);
      }
    }

    if(event is EmailChanged){
      User user = await  userStream.last;
      if(user != null){
        user.email = event.email;
        userSink.add(user);
      }else{
        user = User(email: event.email);
        userSink.add(user);
      }
    }

    if (event is LoggedIn) {
//       event.
      yield AuthenticatedState();
    }

    if (event is LoggedOut) {
      yield AuthLoadingState();

      await userRepository.logOut();
      yield UnauthenticatedState();
    }
  }
}
