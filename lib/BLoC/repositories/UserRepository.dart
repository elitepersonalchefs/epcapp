import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';

enum LoginType { Google, Facebook, Email }

class UserRepository {
  final GoogleSignIn _googleSignIn = GoogleSignIn(scopes: ['email']);
  final FacebookLogin _facebookLogin = FacebookLogin();
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  Future<AuthResult> login(LoginType type, String username,
      String password) async {
    AuthResult user;
    switch (type) {
      case LoginType.Facebook:
        {
          final login = await _facebookLogin.logIn(['email', 'profile']);
          final authCreds = FacebookAuthProvider.getCredential(
              accessToken: login.accessToken.token);
          user = (await _firebaseAuth.signInWithCredential(authCreds));
          break;
        }
      case LoginType.Google:
        final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
        final GoogleSignInAuthentication googleAuth =
        await googleUser.authentication;

        final AuthCredential cred = GoogleAuthProvider.getCredential(
            idToken: googleAuth.idToken, accessToken: googleAuth.accessToken);

        user = (await _firebaseAuth.signInWithCredential(cred));
        break;
      case LoginType.Email:
      // TODO: Handle this case.
        user = await _firebaseAuth.signInWithEmailAndPassword(
            email: username, password: password);
        break;
    }
    return user;
  }

  Future<AuthResult> createWithEmail(String username,
      String password) async{
    final AuthResult user = await _firebaseAuth.createUserWithEmailAndPassword(email: username, password: password);
    return user;
  }

  Future<bool> isLoggedIn() async {
    final user = await _firebaseAuth.currentUser();
    if (user == null) {
      return false;
    }
    return true;
  }

  logOut() async {
    await _firebaseAuth.signOut();
  }

  Future<FirebaseUser> getUser() async {
    final user = await _firebaseAuth.currentUser();
    return user;
  }
}
