import 'package:equatable/equatable.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:meta/meta.dart';

abstract class AuthenticationState implements Equatable {
  @override
  List<Object> get props => [];
}

class UninitializedAuthState extends AuthenticationState {}

class AuthenticatedState extends AuthenticationState {
  final AuthResult user;

  AuthenticatedState({this.user});

  @override
  List<Object> get props => [user];

  @override
  String toString() => 'User Authenticated!  user: ${user?.user?.email}';
}

class AuthenticationFailedState extends AuthenticationState{
  final Exception error;

  AuthenticationFailedState({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'User Authentication failed! $error';
}

class UnauthenticatedState extends AuthenticationState {}

class AuthLoadingState extends AuthenticationState {}

