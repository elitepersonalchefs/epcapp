import 'package:flutter_driver/driver_extension.dart';
import 'package:flutter_driver/flutter_driver.dart';
import 'package:epcapp/main.dart' as app;
import 'package:test/test.dart';

void main() {

  enableFlutterDriverExtension();
  app.main();

  group('EPC App',(){
    final buttonText = find.text('Been here before? Sign In');


    FlutterDriver driver;
    
    setUpAll(() async{
      driver = await FlutterDriver.connect();
    });

    tearDown(() async{
      if(driver != null){

      }
    });

    test('basic test', () async{
      expect(await driver.getText(buttonText),"Been here before? Sign In");
    });

  });
}
